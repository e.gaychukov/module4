﻿using System;
using System.Linq;

namespace M4
{
    public class Module4
    {
        static void Main(string[] args)
        {
            Module4 module4 = new Module4();
            Func<double, double> func = x => Math.Pow(x, 3) + 5 * Math.Pow(x, 2) - 4 * x - 3;
            Console.WriteLine(module4.Task_7(func, -4, 4, 0.001));

            int[] array = new int[] { 2, -4, 4, 0, -9, 10 };
            int[] array1 = new int[] { 2, -4, 4, 0, -9};

            Console.WriteLine(module4.Task_1_A(array));
            Console.WriteLine(module4.Task_1_B(array));
            Console.WriteLine(module4.Task_1_C(array));
            Console.WriteLine(module4.Task_1_D(array));
            Array.ForEach(module4.Task_2(array1, array), Console.WriteLine);
        }


        public int Task_1_A(int[] array)
        {
            if(array != null && array.Length != 0)
            {
                return array.Max();
            }
            throw new ArgumentNullException("array", "is emplty or null");
        }

        public int Task_1_B(int[] array)
        {
            if (array != null && array.Length != 0)
            {
                return array.Min();
            }
            throw new ArgumentNullException("array", "is emplty or null");
        }

        public int Task_1_C(int[] array)
        {
            if (array != null && array.Length != 0)
            {
                return array.Sum();
            }
            throw new ArgumentNullException("array", "is emplty or null");
        }

        public int Task_1_D(int[] array)
        {
            if (array != null && array.Length != 0)
            {
                return array.Max() - array.Min();
            }
            throw new ArgumentNullException("array", "is emplty or null");
        }

        public void Task_1_E(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array", "is emplty or null");
            }
            int max = array.Max();
            int min = array.Min();
            for (int i = 0; i < array.Length; i++)
            {
                if (i % 2 == 0)
                {
                    array[i] += max;
                }
                else
                {
                    array[i] -= min;
                }
            }
        }

        public int Task_2(int a, int b, int c)
        {
            return a + b + c;
        }

        public int Task_2(int a, int b)
        {
            return a + b;
        }

        public double Task_2(double a, double b, double c)
        {
            return a + b + c;
        }

        public string Task_2(string a, string b)
        {
            return a + b;
        }

        public int[] Task_2(int[] a, int[] b)
        {
            int[] result = new int[Math.Max(a.Length, b.Length)];
            for(int i = 0; i < result.Length; i++)
            {
                try
                {
                    result[i] = a[i] + b[i];
                }
                catch (IndexOutOfRangeException) 
                {
                    result[i] = a.Length > b.Length ? a[i] : b[i];
                }
            }
            return result;
        }

        public void Task_3_A(ref int a, ref int b, ref int c)
        {
            a += 10;
            b += 10;
            c += 10;
        }

        public void Task_3_B(double radius, out double length, out double square)
        {
            if(radius < 0)
            {
                throw new ArgumentException("is negative", "radius");
            }
            length = 2 * Math.PI * radius;
            square = Math.PI * Math.Pow(radius, 2);
        }

        public void Task_3_C(int[] array, out int maxItem, out int minItem, out int sumOfItems)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array", "is emplty or null");
            }
            maxItem = array.Max();
            minItem = array.Min();
            sumOfItems = array.Sum();
        }

        public (int, int, int) Task_4_A((int, int, int) numbers)
        {
            return (numbers.Item1 + 10, numbers.Item2 + 10, numbers.Item3 + 10);
        }

        public (double, double) Task_4_B(double radius)
        {
            if (radius < 0)
            {
                throw new ArgumentException("is negative", "radius");
            }
            return (2 * Math.PI * radius, Math.PI * Math.Pow(radius, 2));
        }

        public (int, int, int) Task_4_C(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array", "is emplty or null");
            }
            return (array.Min(), array.Max(), array.Sum());
        }

        public void Task_5(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array", "is emplty or null");
            }
            for(int i = 0; i < array.Length; i++)
            {
                array[i] += 5;
            }
        }

        public void Task_6(int[] array, SortDirection direction)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array", "is emplty or null");
            }
            int direct = direction == SortDirection.Ascending ? 1 : -1;
            Array.Sort(array, (a, b) =>  (a - b) * direct);
        }        

        public  double Task_7(Func<double, double> func, double x1, double x2, double e, double result = 0)
        {
            result = (x1 + x2) / 2;

            if (Math.Abs(x2 - x1) < e)
            {
                return result;
            }
            else
            {
                if(Math.Sign(func(x1)) != Math.Sign(func(result)))
                {
                    return Task_7(func, x1, result, e);
                }
                else
                {
                    return Task_7(func, result, x2, e);
                }
            }
        }
    }
}
